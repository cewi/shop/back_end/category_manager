package main

import (
  application "gitlab.com/cewi/shop/back_end/category_manager/command/src"
  "log"
)

func main() {
  log.Println("Inicializando serviço Category Command")
  application.NewApplication().Init()
}
