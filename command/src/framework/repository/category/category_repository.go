package repository_category

import (
  "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework"
  "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework/repository/database"
  "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework/repository/mapper"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

const (
  databaseName = "category_manager"
  tableName    = "category"
)

type ICategoryRepository interface {
  Save(mapper repository_mapper.CreateCategoryMapper) (*string, *framework.Error)
  Update(mapper repository_mapper.UpdateCategoryMapper) *framework.Error
  Delete(uuid string) *framework.Error
}

type categoryRepository struct {
  GenericRepository[repository_mapper.CreateCategoryMapper]
}

func (c categoryRepository) Save(mapper repository_mapper.CreateCategoryMapper) (*string, *framework.Error) {
  save, err := c.GenericRepository.Save(mapper)
  if err != nil {
    errRepo := framework.NewError(err.Error(), int64(2001))
    return nil, &errRepo
  }
  return &save, nil
}

func (c categoryRepository) Update(mapper repository_mapper.UpdateCategoryMapper) *framework.Error {
  err := c.GenericRepository.Update(mapper.Uuid, mapper)
  if err != nil {
    newError := framework.NewError(err.Error(), int64(2002))
    return &newError
  }
  return nil
}

func (c categoryRepository) Delete(uuid string) *framework.Error {
  hex, _ := primitive.ObjectIDFromHex(uuid)
  err := c.GenericRepository.Delete(hex)
  if err != nil {
    newError := framework.NewError(err.Error(), int64(2003))
    return &newError
  }
  return nil
}

func NewCategoryRepository(mongoDataSource repository_database.IMongoDataSource) ICategoryRepository {
  return categoryRepository{
    GenericRepository[repository_mapper.CreateCategoryMapper]{
      MongoDataSource: mongoDataSource,
      DatabaseName:    databaseName,
      TableName:       tableName,
    },
  }
}
