package repository_category

import (
  "context"
  repository_database "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework/repository/database"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "time"
)

type GenericRepository[T any] struct {
  MongoDataSource repository_database.IMongoDataSource
  DatabaseName    string
  TableName       string
}

func (c GenericRepository[T]) Save(mapper T) (string, error) {
  connect, _ := c.MongoDataSource.Connect()
  defer c.MongoDataSource.Close(connect)
  collection := c.MongoDataSource.DataSource(connect, c.DatabaseName, c.TableName)
  ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
  defer cancel()
  response, err := collection.InsertOne(ctx, mapper)
  if err != nil {
    return "", err
  }
  hex := response.InsertedID.(primitive.ObjectID).Hex()
  return hex, nil
}

func (c GenericRepository[T]) Update(id primitive.ObjectID, mapper interface{}) error {
  connect, _ := c.MongoDataSource.Connect()
  defer c.MongoDataSource.Close(connect)
  collection := c.MongoDataSource.DataSource(connect, c.DatabaseName, c.TableName)
  ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
  defer cancel()
  filter := bson.M{"_id": bson.M{"$eq": id}}
  _, err := collection.UpdateOne(ctx, filter, bson.M{"$set": mapper})
  if err != nil {
    return err
  }
  return nil

}

func (c GenericRepository[T]) Delete(id primitive.ObjectID) error {
  connect, _ := c.MongoDataSource.Connect()
  defer c.MongoDataSource.Close(connect)
  collection := c.MongoDataSource.DataSource(connect, c.DatabaseName, c.TableName)
  ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
  defer cancel()
  filter := bson.M{"_id": id}
  _, err := collection.DeleteOne(ctx, filter)
  if err != nil {
    return err
  }
  return nil

}
