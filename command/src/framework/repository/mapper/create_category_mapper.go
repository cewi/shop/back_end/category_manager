package repository_mapper

import (
  "time"
)

type CreateCategoryMapper struct {
  Description    string    `bson:"description"`
  CreateAt       time.Time `bson:"created_at"`
  Status         bool      `bson:"status"`
  ParentCategory string    `bson:"parent_category"`
}
