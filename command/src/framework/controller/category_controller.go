package framework_controller

import (
  "context"
  "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework/proto_buffer"
  repository_category "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework/repository/category"
  repository_database "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework/repository/database"
  usecasecategory "gitlab.com/cewi/shop/back_end/category_manager/command/src/use_case/category"
)

type categoryController struct {
  categoryInteractor usecasecategory.ICategoryInteractor
}

func (c *categoryController) Create(_ context.Context, request *proto_buffer.CreateCategoryRequest) (*proto_buffer.CreateCategoryResponse, error) {
  return c.categoryInteractor.Create(request)
}

func (c *categoryController) Update(_ context.Context, request *proto_buffer.UpdateCategoryRequest) (*proto_buffer.UpdateCategoryResponse, error) {
  return c.categoryInteractor.Update(request)
}

func (c *categoryController) Delete(_ context.Context, request *proto_buffer.DeleteCategoryRequest) (*proto_buffer.DeleteCategoryResponse, error) {
  return c.categoryInteractor.Delete(request)
}

func NewCategoryController() categoryController {
  mongoDataSource := repository_database.NewMongoDataSource()
  categoryRepository := repository_category.NewCategoryRepository(mongoDataSource)
  categoryInteractor := usecasecategory.NewCategoryInteractor(categoryRepository)
  return categoryController{categoryInteractor: categoryInteractor}
}
