package framework

import "errors"

type Error struct {
  error
  code int64
}

func (e Error) Error() string {
  return e.error.Error()
}

func (e Error) Code() int64 {
  return e.code
}

func NewError(message string, code int64) Error {
  return Error{
    error: errors.New(message),
    code:  code,
  }
}
