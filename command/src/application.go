package application

import (
  frameworkcontroller "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework/controller"
  "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework/proto_buffer"
  "google.golang.org/grpc"
  "google.golang.org/grpc/reflection"
  "log"
  "net"
  "os"
)

type application struct {
  categoryCommandServer proto_buffer.CategoryCommandServer
}

const (
  TCP = "tcp"
)

func (a *application) Init() {

  grpcServer := grpc.NewServer()
  proto_buffer.RegisterCategoryCommandServer(grpcServer, a.categoryCommandServer)
  reflection.Register(grpcServer)

  port := os.Getenv("GRPC_PORT")
  listener, err := net.Listen(TCP, ":"+port)
  if err != nil {
    log.Fatalf(err.Error())
  }
  log.Printf("Applicação Category Command, inicializada com sucesso na porta: %v\n", port)
  errs := grpcServer.Serve(listener)
  if errs != nil {
    log.Fatalf(err.Error())
  }
}

func NewApplication() *application {
  categoryController := frameworkcontroller.NewCategoryController()
  return &application{
    categoryCommandServer: &categoryController,
  }
}
