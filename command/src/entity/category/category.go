package entity_category

import (
  "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework"
)

type ICategory interface {
  Description() string
  ParentCategory() string
  Status() bool
}

type category struct {
  description    string
  parentCategory *string
  status         bool
}

func NewCategory(description string, status bool) (ICategory, *framework.Error) {
  return NewCategoryWithParent(description, status, "")
}

func NewCategoryWithParent(description string, status bool, parentCategory string) (ICategory, *framework.Error) {
  category := category{
    description: description,
    status:      status,
  }
  if len(parentCategory) > 0 {
    category.parentCategory = &parentCategory
  }
  err := category.isValidate()
  if err != nil {
    return nil, err
  }
  return category, nil
}

func (c category) Description() string {
  return c.description
}

func (c category) ParentCategory() string {
  return *c.parentCategory
}

func (c category) Status() bool {
  return c.status
}

func (c category) isValidate() *framework.Error {
  if len(c.description) == 0 {
    newError := framework.NewError("descrição não informada", 1001)
    return &newError
  }
  return nil
}
