package entity_category

import (
  "github.com/stretchr/testify/assert"
  "testing"
)

func Test_devera_criar_uma_categoria_com_exito(t *testing.T) {
  assertions := assert.New(t)
  category, err := NewCategory("Cerial", true)

  assertions.Nil(err)
  assertions.Equal("Cerial", category.Description())
}

func Test_devera_validar_descricao_vazia(t *testing.T) {
  assertions := assert.New(t)
  category, err := NewCategory("", false)

  assertions.NotNil(err)
  assertions.EqualError(err, "descrição não informada")
  assertions.Equal(1001, err.Code())
  assertions.Nil(category)

}

func Test_devera_categoria_com_pai(t *testing.T) {
  assertions := assert.New(t)
  parentCategory := "6d77e624-9a62-424b-a4a3-b307ce2ba023"
  category, err := NewCategoryWithParent("Ceriais", true, parentCategory)

  assertions.Nil(err)
  assertions.Equal("Ceriais", category.Description())

  parent := category.ParentCategory()
  assertions.Equal(parentCategory, parent)
}
