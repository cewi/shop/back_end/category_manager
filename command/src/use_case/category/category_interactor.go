package usecase_category

import (
  entity_category "gitlab.com/cewi/shop/back_end/category_manager/command/src/entity/category"
  "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework"
  "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework/proto_buffer"
  repositorycategory "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework/repository/category"
  repository_mapper "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework/repository/mapper"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "time"
)

type ICategoryInteractor interface {
  Create(inputBoundary *proto_buffer.CreateCategoryRequest) (*proto_buffer.CreateCategoryResponse, error)
  Update(request *proto_buffer.UpdateCategoryRequest) (*proto_buffer.UpdateCategoryResponse, error)
  Delete(request *proto_buffer.DeleteCategoryRequest) (*proto_buffer.DeleteCategoryResponse, error)
}

type categoryInteractor struct {
  categoryRepository repositorycategory.ICategoryRepository
}

func NewCategoryInteractor(
    categoryRepository repositorycategory.ICategoryRepository,
) ICategoryInteractor {
  return &categoryInteractor{categoryRepository: categoryRepository}
}

func (c categoryInteractor) Create(inputBoundary *proto_buffer.CreateCategoryRequest) (*proto_buffer.CreateCategoryResponse, error) {
  category, err := c.toEntity(inputBoundary)
  if err != nil {
    return c.createFail(err), nil
  }
  categoryMapper := repository_mapper.CreateCategoryMapper{
    Description:    category.Description(),
    CreateAt:       time.Now(),
    Status:         category.Status(),
    ParentCategory: category.ParentCategory(),
  }
  _, errRepo := c.categoryRepository.Save(categoryMapper)
  if errRepo != nil {
    return c.createFail(errRepo), nil
  }
  return &proto_buffer.CreateCategoryResponse{Message: "categoria " + category.Description() + ", registrada"}, nil
}

func (c categoryInteractor) Update(updateCategory *proto_buffer.UpdateCategoryRequest) (*proto_buffer.UpdateCategoryResponse, error) {
  category, err := c.toUpdateEntity(updateCategory)
  if err != nil {
    return c.updateFail(err), nil
  }

  hex, _ := primitive.ObjectIDFromHex(updateCategory.Uuid)

  updateCategoryMapper := repository_mapper.UpdateCategoryMapper{
    Uuid:           hex,
    Description:    category.Description(),
    Status:         category.Status(),
    UpdateAt:       time.Now(),
    ParentCategory: category.ParentCategory(),
  }
  errRepo := c.categoryRepository.Update(updateCategoryMapper)
  if errRepo != nil {
    return c.updateFail(errRepo), nil
  }
  return &proto_buffer.UpdateCategoryResponse{Message: "categoria " + category.Description() + ", alterada"}, nil
}

func (c categoryInteractor) Delete(category *proto_buffer.DeleteCategoryRequest) (*proto_buffer.DeleteCategoryResponse, error) {
  errRepo := c.categoryRepository.Delete(category.Uuid)
  // FIXME validar se existe algum produto vinculado a categoria.
  // FIXME se sim, não poderá ser excuido a categoria. excluir os produtos vinculados.
  if errRepo != nil {
    return c.deleteFail(errRepo), nil
  }
  return &proto_buffer.DeleteCategoryResponse{Message: "categoria, deletada"}, nil
}

func (c categoryInteractor) createFail(err *framework.Error) *proto_buffer.CreateCategoryResponse {
  return &proto_buffer.CreateCategoryResponse{
    Error: &proto_buffer.Error{
      Code:  err.Code(),
      Error: err.Error(),
    },
  }
}

func (c categoryInteractor) deleteFail(err *framework.Error) *proto_buffer.DeleteCategoryResponse {
  return &proto_buffer.DeleteCategoryResponse{
    Error: &proto_buffer.Error{
      Code:  err.Code(),
      Error: err.Error(),
    },
  }
}

func (c categoryInteractor) updateFail(err *framework.Error) *proto_buffer.UpdateCategoryResponse {
  return &proto_buffer.UpdateCategoryResponse{
    Error: &proto_buffer.Error{
      Code:  err.Code(),
      Error: err.Error(),
    },
  }
}

func (c categoryInteractor) toEntity(inputBoundary *proto_buffer.CreateCategoryRequest) (entity_category.ICategory, *framework.Error) {
  return entity_category.NewCategoryWithParent(inputBoundary.GetDescription(), inputBoundary.GetStatus(), inputBoundary.GetParentCategory())
}

func (c categoryInteractor) toUpdateEntity(createCategory *proto_buffer.UpdateCategoryRequest) (entity_category.ICategory, *framework.Error) {
  return entity_category.NewCategoryWithParent(createCategory.GetDescription(), createCategory.GetStatus(), createCategory.GetParentCategory())
}
