package usecase_category

import (
  "github.com/stretchr/testify/assert"
  "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework"
  "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework/proto_buffer"
  "gitlab.com/cewi/shop/back_end/category_manager/command/src/use_case/category/mock"
  "testing"
)

func Test_devera_criar_uma_categoria_com_pai(t *testing.T) {
  assertions := assert.New(t)

  repository := new(mock.CategoryRepositoryMock)
  interactor := NewCategoryInteractor(repository)

  var err *framework.Error
  uuid := "e32f9c61-aa16-4fde-abc8-97d4557fae75"
  repository.On("Save").Return(&uuid, err)

  input := proto_buffer.CreateCategoryRequest{
    Description: "Computadores",
    OptionalParentCategory: &proto_buffer.CreateCategoryRequest_ParentCategory{
      ParentCategory: uuid,
    },
    Status: true,
  }

  catetoryOutput, _ := interactor.Create(&input)
  assertions.NotNil(catetoryOutput)
  assertions.Equal("categoria Computadores, registrada", catetoryOutput.Message)
  repository.AssertNumberOfCalls(t, "Save", 1)

}

func Test_devera_validar_a_descricao_categoria(t *testing.T) {
  assertions := assert.New(t)

  repository := new(mock.CategoryRepositoryMock)
  interactor := NewCategoryInteractor(repository)

  input := proto_buffer.CreateCategoryRequest{
    OptionalParentCategory: &proto_buffer.CreateCategoryRequest_ParentCategory{
      ParentCategory: "e32f9c61-aa16-4fde-abc8-97d4557fae75",
    },
    Status: true,
  }

  catetoryOutput, _ := interactor.Create(&input)
  assertions.NotNil(catetoryOutput)
  assertions.NotNil(catetoryOutput.Error)
  assertions.Equal("descrição não informada", catetoryOutput.Error.Error)
  assertions.Equal(int64(1001), catetoryOutput.Error.Code)

}

func Test_devera_validar_erro_caso_ocorra_ao_salvar_categoria(t *testing.T) {

  assertions := assert.New(t)

  repository := new(mock.CategoryRepositoryMock)
  interactor := NewCategoryInteractor(repository)

  input := proto_buffer.CreateCategoryRequest{
    Description: "Computadores",
    OptionalParentCategory: &proto_buffer.CreateCategoryRequest_ParentCategory{
      ParentCategory: "e32f9c61-aa16-4fde-abc8-97d4557fae75",
    },
    Status: true,
  }

  err := framework.NewError("Erro ao tentar salvar no banco de dados", 2001)
  var arguments *string
  repository.On("Save").Return(arguments, &err)

  catetoryOutput, _ := interactor.Create(&input)
  assertions.NotNil(catetoryOutput)
  assertions.NotNil(catetoryOutput.Error)
  assertions.Equal("Erro ao tentar salvar no banco de dados", catetoryOutput.Error.Error)
  assertions.Equal(int64(2001), catetoryOutput.Error.Code)
  repository.AssertNumberOfCalls(t, "Save", 1)
}

func Test_devera_autualizar_uma_categoria_com_pai(t *testing.T) {
  assertions := assert.New(t)

  repository := new(mock.CategoryRepositoryMock)
  interactor := NewCategoryInteractor(repository)

  var err *framework.Error
  repository.On("Update").Return(err)

  input := proto_buffer.UpdateCategoryRequest{
    Uuid:        "e32f9c61-aa16-4fde-abc8-97d4557fae75",
    Description: "Computadores",
    OptionalParentCategory: &proto_buffer.UpdateCategoryRequest_ParentCategory{
      ParentCategory: "e32f9c61-aa16-4fde-abc8-97d4557fae75",
    },
    Status: true,
  }

  catetoryOutput, _ := interactor.Update(&input)
  assertions.NotNil(catetoryOutput)
  assertions.Equal("categoria Computadores, alterada", catetoryOutput.Message)
  repository.AssertNumberOfCalls(t, "Update", 1)

}

func Test_devera_validar_a_descricao_categoria_ao_atualizar(t *testing.T) {
  assertions := assert.New(t)

  repository := new(mock.CategoryRepositoryMock)
  interactor := NewCategoryInteractor(repository)

  input := proto_buffer.UpdateCategoryRequest{
    Uuid: "e32f9c61-aa16-4fde-abc8-97d4557fae75",
    OptionalParentCategory: &proto_buffer.UpdateCategoryRequest_ParentCategory{
      ParentCategory: "e32f9c61-aa16-4fde-abc8-97d4557fae75",
    },
    Status: true,
  }

  catetoryOutput, _ := interactor.Update(&input)
  assertions.NotNil(catetoryOutput)
  assertions.NotNil(catetoryOutput.Error)
  assertions.Equal("descrição não informada", catetoryOutput.Error.Error)
  assertions.Equal(int64(1001), catetoryOutput.Error.Code)
  repository.AssertNumberOfCalls(t, "Update", 0)

}

func Test_devera_validar_erro_caso_ocorra_ao_salvar_categoria_ao_alterar(t *testing.T) {
  assertions := assert.New(t)

  repository := new(mock.CategoryRepositoryMock)
  interactor := NewCategoryInteractor(repository)

  err := framework.NewError("erro ao tentar alterar categoria", 2002)
  repository.On("Update").Return(&err)

  input := proto_buffer.UpdateCategoryRequest{
    Uuid:        "e32f9c61-aa16-4fde-abc8-97d4557fae75",
    Description: "Computadores",
    OptionalParentCategory: &proto_buffer.UpdateCategoryRequest_ParentCategory{
      ParentCategory: "e32f9c61-aa16-4fde-abc8-97d4557fae75",
    },
    Status: true,
  }

  catetoryOutput, _ := interactor.Update(&input)
  assertions.NotNil(catetoryOutput)
  assertions.NotNil(catetoryOutput.Error)
  assertions.Equal("erro ao tentar alterar categoria", catetoryOutput.Error.Error)
  assertions.Equal(int64(2002), catetoryOutput.Error.Code)
  repository.AssertNumberOfCalls(t, "Update", 1)

}

func Test_devera_deletar_a_categoria(t *testing.T) {
  assertions := assert.New(t)

  repository := new(mock.CategoryRepositoryMock)
  interactor := NewCategoryInteractor(repository)

  input := proto_buffer.DeleteCategoryRequest{
    Uuid: "e32f9c61-aa16-4fde-abc8-97d4557fae75",
  }

  var err *framework.Error
  repository.On("Delete").Return(err)

  catetoryOutput, _ := interactor.Delete(&input)
  assertions.NotNil(catetoryOutput)
  assertions.Equal("categoria, deletada", catetoryOutput.Message)
  repository.AssertNumberOfCalls(t, "Delete", 1)

}

func Test_devera_validar_erro_ao_deletar_a_categoria(t *testing.T) {
  assertions := assert.New(t)

  repository := new(mock.CategoryRepositoryMock)
  interactor := NewCategoryInteractor(repository)

  input := proto_buffer.DeleteCategoryRequest{
    Uuid: "e32f9c61-aa16-4fde-abc8-97d4557fae75",
  }

  err := framework.NewError("erro ao tentar deletar a categoria", 2003)
  repository.On("Delete").Return(&err)

  catetoryOutput, _ := interactor.Delete(&input)
  assertions.NotNil(catetoryOutput)
  assertions.NotNil(catetoryOutput.Error)
  assertions.Equal("erro ao tentar deletar a categoria", catetoryOutput.Error.Error)
  assertions.Equal(int64(2003), catetoryOutput.Error.Code)
  repository.AssertNumberOfCalls(t, "Delete", 1)

}
