package mock

import (
  "github.com/stretchr/testify/mock"
  "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework"
  repository_mapper "gitlab.com/cewi/shop/back_end/category_manager/command/src/framework/repository/mapper"
)

type CategoryRepositoryMock struct {
  mock.Mock
}

func (mock *CategoryRepositoryMock) Save(mapper repository_mapper.CreateCategoryMapper) (*string, *framework.Error) {
  args := mock.Called()
  result := args.Get(0)
  err := args.Error(1).(*framework.Error)
  return (result).(*string), err
}

func (mock *CategoryRepositoryMock) Update(mapper repository_mapper.UpdateCategoryMapper) *framework.Error {
  args := mock.Called()
  result := args.Get(0)
  return result.(*framework.Error)
}

func (mock *CategoryRepositoryMock) Delete(uuid string) *framework.Error {
  args := mock.Called()
  result := args.Get(0)
  return result.(*framework.Error)
}
