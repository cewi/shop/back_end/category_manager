# Projeto Category Service Command

### Comando para criar o projeto em Go

```shell
 go mod init gitlab.com/cewi/shop/back_end/category_service/command 
```

# Go Mod

```shell
go mod tidy
```

# Comandos coverage

  ```shell
  go test ./...
  ```

  ```shell
  go test -coverprofile=coverage.out ./... && go tool cover -func=coverage.out
  ```

# Comando para compilar

  ```shell
  go build
  ```

# Instalando Protoc

```shell
sudo apt install -y protobuf-compiler
sudo apt install golang-goprotobuf-dev
```

## Configurar as Variáveis

```shell
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export PATH="$PATH:/usr/bin/protoc"
export PATH="$PATH:$GOROOT:$GOPATH:$GOBIN"
```

# Instalando Protoc GRPC

```shell
sudo apt install -y protobuf-compiler
sudo apt install golang-goprotobuf-dev
```

### Compilar o arquivo proto

```shell
protoc --proto_path=proto proto/*.proto --go_out=plugins=grpc:src/framework/proto_buffer
```

### Instalar o Evans para teste o servidor grpc

https://github.com/ktr0731/evans

```shell
go install github.com/ktr0731/evans@latest
```

## Comandos Evans

```shell
evans -r -p 55080
service ServiceName
```

# Variáveis de ambiente da aplicação

```shell
export GRPC_CATEGORY_COMMAND_PORT=55080
export DATABASE_MONGODB_URL=mongodb://admin:admin123@127.0.0.1:27017
```

# Código de erros

- 1000 validações dos dados da categoria
    - 1001:  descrição não informada
- 2000 erros durante a persistêcia dos dados
    - 2001: erro ao tentar salvar a categoria
    - 2002: erro ao tentar alterar a categoria
    - 2003: erro ao tentar deletar a categoria

# K6

linux https://k6.io/docs/getting-started/installation/

loging k6 cloud: https://app.k6.io/tests/new/cli

```shell
snap install k6
```

```shell
echo runner cloud
k6 run -o cloud k6/create/2_load.js
echo runner local
k6 run k6/create/2_load.js
```

```shell
k6 run -o cloud k6/create/1_smoke.js
k6 run -o cloud k6/create/2_load.js
k6 run -o cloud k6/create/3_ramp_up_scenario.js
k6 run -o cloud k6/create/4_stress.js
k6 run -o cloud k6/create/5_stress.js
```

```shell
k6 run -o cloud k6/update/1_smoke.js
k6 run -o cloud k6/update/2_load.js
k6 run -o cloud k6/update/3_ramp_up_scenario.js
k6 run -o cloud k6/update/4_stress.js
k6 run -o cloud k6/update/5_stress.js
```

```shell
k6 run -o cloud k6/delete/1_smoke.js
k6 run -o cloud k6/delete/2_load.js
k6 run -o cloud k6/delete/3_ramp_up_scenario.js
k6 run -o cloud k6/delete/4_stress.js
k6 run -o cloud k6/delete/5_stress.js
```

# docker publish

GRPC_CATEGORY_COMMAND_PORT: 9002

```shell
echo senha | docker login --username lukewarecewi --password-stdin
docker build --tag lukewarecewi/category_service_command:1.0.1 .
docker tag lukewarecewi/category_service_command:1.0.1 lukewarecewi/category_service_command:1.0.1 
docker push lukewarecewi/category_service_command:1.0.1
```

# Backlog

[  ] instalar a aplicação no kubernets

[  ] configurar gitlab.ci.yml

[  ] Configurar influxdb via docker

[  ] Configurar grafana via docker

# docker create network

```shell
docker network create -d bridge category_net
```

# Verificar se a replicação do banco foi criada

```shell
docker exec -it mongo_primary_category_command mongosh --eval "rs.status()"
```