import grpc from 'k6/net/grpc';
import {check, sleep} from 'k6';

const client = new grpc.Client();
client.load(['../../proto'], 'category_command.proto');

export const options = {
  ext: {
    loadimpact: {
      projectID: 3603316,
      name: "delete_stress_2"
    }
  },
  stages: [
    {duration: '10s', target: 40},
    {duration: '1m', target: 40},
    {duration: '10s', target: 50},
    {duration: '3m', target: 50},
    {duration: '10s', target: 40},
    {duration: '3m', target: 40},
    {duration: '10s', target: 0},
  ],
  thresholds: {
    grpc_req_duration: [{threshold: 'p(100)<3000'}],
    iteration_duration: [{threshold: 'p(100)<10000'}],
  },
};

export default function () {

  client.connect('host.docker.internal:9001', {
    plaintext: true,
    reflect: true,
    timeout: "10s"
  });

  const data = {
    uuid: "633505cffa98879f88e393a8",
  };
  const responseUpdate1 = client.invoke('category_command.CategoryCommand/Delete', data);
  const responseUpdate2 = client.invoke('category_command.CategoryCommand/Delete', data);
  const responseUpdate3 = client.invoke('category_command.CategoryCommand/Delete', data);
  const responseUpdate4 = client.invoke('category_command.CategoryCommand/Delete', data);

  check(responseUpdate1, {
    'delete is ok 1': (r) => r && r.status === grpc.StatusOK,
  });
  check(responseUpdate2, {
    'delete is ok 2': (r) => r && r.status === grpc.StatusOK,
  });
  check(responseUpdate3, {
    'delete is ok 3': (r) => r && r.status === grpc.StatusOK,
  });
  check(responseUpdate4, {
    'delete is ok 4': (r) => r && r.status === grpc.StatusOK,
  });

  client.close();
  sleep(1);
};