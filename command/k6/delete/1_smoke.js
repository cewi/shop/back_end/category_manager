import grpc from 'k6/net/grpc';
import {check, sleep} from 'k6';

const client = new grpc.Client();
client.load(['../../proto'], 'category_command.proto');

export const options = {
  ext: {
    loadimpact: {
      projectID: 3603316,
      name: "delete_smoke"
    }
  },
  vus: 1,
  duration: '1m',
  thresholds: {
    grpc_req_duration: [{threshold: 'p(99)<1500'}],
    iteration_duration: [{threshold: 'p(99)<1500'}],
  },
};

export default function () {

  client.connect('host.docker.internal:9001', {
    plaintext: true,
    reflect: true,
    timeout: "10s"
  });

  const data = {
    uuid: "635828cae8d052f773059c8a",
  };
  const responseUpdate = client.invoke('category_command.CategoryCommand/Delete', data);

  check(responseUpdate, {
    'delete is ok': (r) => r && r.status === grpc.StatusOK,
  });

  client.close();
  sleep(1);
};