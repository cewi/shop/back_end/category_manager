import grpc from 'k6/net/grpc';
import {check} from 'k6';

const client = new grpc.Client();
client.load(['../../proto'], 'category_command.proto');

export const options = {
  ext: {
    loadimpact: {
      projectID: 3603316,
      name: "create_streess_2"
    }
  },
  stages: [
    {duration: '1m', target: 30},
    {duration: '2m', target: 30},
    {duration: '1m', target: 35},
    {duration: '2m', target: 35},
    {duration: '1m', target: 45},
    {duration: '2m', target: 45},
    {duration: '1m', target: 50},
    {duration: '2m', target: 0},
  ],
  thresholds: {
    grpc_req_duration: [{threshold: 'p(95)<1000'}],
    iteration_duration: [{threshold: 'p(95)<3000'}],
  },
};

export default function () {

  client.connect('host.docker.internal:9001', {
    plaintext: true,
    reflect: true,
    timeout: "10s"
  });

  const data = {
    description: "Categoria",
    status: true,
    parentCategory: "ec67570d-8047-4a19-be98-496dac95c81c",
  };

  const responseCreate1 = client.invoke('category_command.CategoryCommand/Create', data);
  const responseCreate2 = client.invoke('category_command.CategoryCommand/Create', data);
  const responseCreate3 = client.invoke('category_command.CategoryCommand/Create', data);
  const responseCreate4 = client.invoke('category_command.CategoryCommand/Create', data);

  check(responseCreate1, {
    'create is ok 1': (r) => r && r.status === grpc.StatusOK,
  });
  check(responseCreate2, {
    'create is ok 1': (r) => r && r.status === grpc.StatusOK,
  });
  check(responseCreate3, {
    'create is ok 1': (r) => r && r.status === grpc.StatusOK,
  });
  check(responseCreate4, {
    'create is ok 1': (r) => r && r.status === grpc.StatusOK,
  });


  client.close();
};