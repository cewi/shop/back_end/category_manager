import grpc from 'k6/net/grpc';
import {check} from 'k6';

const client = new grpc.Client();
client.load(['../../proto'], 'category_command.proto');

export const options = {
  ext: {
    loadimpact: {
      projectID: 3603316,
      name: "create_load"
    }
  },
  stages: [
    {duration: '2m', target: 50},
    {duration: '5m', target: 50},
    {duration: '2m', target: 0},
  ],
  thresholds: {
    grpc_req_duration: [{threshold: 'p(99)<1500'}],
    iteration_duration: [{threshold: 'p(99)<1500'}],
  },
};

export default function () {

  client.connect('host.docker.internal:9001', {
    plaintext: true,
    reflect: true,
    timeout: "10s"
  });

  const data = {
    description: "Categoria",
    status: true,
    parentCategory: "ec67570d-8047-4a19-be98-496dac95c81c",
  };
  const responseCreate = client.invoke('category_command.CategoryCommand/Create', data);

  check(responseCreate, {
    'create is ok': (r) => r && r.status === grpc.StatusOK,
  });

  client.close();
};