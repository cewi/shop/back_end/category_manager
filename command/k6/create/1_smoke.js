import grpc from 'k6/net/grpc';
import {check, fail} from 'k6';

const client = new grpc.Client();
client.load(['../../proto'], 'category_command.proto');

export const options = {
  ext: {
    loadimpact: {
      projectID: 3603316,
      name: "create_smoke"
    }
  },
  vus: 50,
  duration: '1m',
  thresholds: {
    grpc_req_duration: [{threshold: 'p(95)<1500'}],
    iteration_duration: [{threshold: 'p(95)<1500'}],
  },
};

export default function () {

  client.connect('host.docker.internal:9001', {
    plaintext: true,
    reflect: true,
    timeout: "10s"
  });

  const data = {
    description: "Eletrônicos",
    status: true,
    parentCategory: "ec67570d-8047-4a19-be98-496dac95c81c",
  };
  const res = client.invoke('category_command.CategoryCommand/Create', data);

  check(res, {
    'create success ok': (r) => {
      return r && r.status === grpc.StatusOK
    },
    'mensage response ok': (r) => {
      return r && r.message.message === "categoria Eletrônicos, registrada"
    },
  });

  if (!res) {
    fail('unexpected response');
  }

  client.close();
};
