package framework_controller

import (
  "context"
  "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/proto_buffer"
  "gitlab.com/cewi/shop/back_end/category_manager/query/src/interface_adapter/adpter_controller"
)

type CategoryController struct {
  categoryAdaperController adapter_controller.ICategoryAdapterController
}

func (c *CategoryController) FindById(_ context.Context, request *proto_buffer.QueryByIdRequest) (*proto_buffer.CategoryResponse, error) {
  return c.categoryAdaperController.FindById(*request)
}
func (c *CategoryController) FindAll(_ context.Context, request *proto_buffer.QueryPaginationRequest) (*proto_buffer.CategoriesResponse, error) {
  return c.categoryAdaperController.FindAll(*request)
}
func (c *CategoryController) FindByParent(_ context.Context, request *proto_buffer.QueryByIdRequest) (*proto_buffer.CategoriesResponse, error) {
  return c.categoryAdaperController.FindByParent(*request)
}
func (c *CategoryController) Search(_ context.Context, request *proto_buffer.QuerySearchRequest) (*proto_buffer.CategoriesResponse, error) {
  return c.categoryAdaperController.Search(*request)
}

func NewCategoryController() CategoryController {
  categoryAdaperController := adapter_controller.NewCategoryAdapterController()
  return CategoryController{categoryAdaperController: categoryAdaperController}
}
