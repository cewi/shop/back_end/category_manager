package framework_repository

import (
  "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/repository/database"
  "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/repository/mapper"
)

const (
  databaseName = "category_manager"
  tableName    = "category"
)

type ICategoryRepository interface {
  FindById(uuid string) (*framework_mapper.CategoryMapper, error)
  FindAll(page int64, size int64) (*framework_mapper.PaginationCategoriesMapper, error)
  FindByParent(uuid string, key string) ([]*framework_mapper.CategoryMapper, error)
  Search(query framework_mapper.QueryMapper) ([]*framework_mapper.CategoryMapper, error)
}

type categoryRepository struct {
  GenericRepository[framework_mapper.CategoryMapper]
}

func (c *categoryRepository) FindById(uuid string) (*framework_mapper.CategoryMapper, error) {
  return c.GenericRepository.FindById(uuid)
}

func (c *categoryRepository) FindByParent(uuid string, key string) ([]*framework_mapper.CategoryMapper, error) {
  return c.GenericRepository.FindAllByCustomized(uuid, key)
}

func (c *categoryRepository) FindAll(page int64, size int64) (*framework_mapper.PaginationCategoriesMapper, error) {
  paginatedData, categoriesMapper, err := c.GenericRepository.FindAll(int(page), int(size))
  if err != nil {
    return nil, err
  }
  return &framework_mapper.PaginationCategoriesMapper{
    Categories: categoriesMapper,
    Next:       paginatedData.Pagination.Next,
    Page:       paginatedData.Pagination.Page,
    Prev:       paginatedData.Pagination.Prev,
    Total:      paginatedData.Pagination.Total,
    PerPage:    paginatedData.Pagination.PerPage,
    TotalPage:  paginatedData.Pagination.TotalPage,
  }, nil
}

func (c *categoryRepository) Search(query framework_mapper.QueryMapper) ([]*framework_mapper.CategoryMapper, error) {
  return c.GenericRepository.Search(query, "$or")
}

func NewCategoryRepository(mongoDataSource repository_database.IMongoDataSource) ICategoryRepository {
  return &categoryRepository{
    GenericRepository[framework_mapper.CategoryMapper]{
      MongoDataSource: mongoDataSource,
      DatabaseName:    databaseName,
      TableName:       tableName,
    },
  }
}
