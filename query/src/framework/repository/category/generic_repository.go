package framework_repository

import (
  "context"
  "encoding/json"
  "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/repository/database"
  "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/repository/mapper"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "time"

  . "github.com/gobeam/mongo-go-pagination"
)

type GenericRepository[T any] struct {
  MongoDataSource repository_database.IMongoDataSource
  DatabaseName    string
  TableName       string
}

func (c GenericRepository[T]) FindById(id string) (*T, error) {
  connect, _ := c.MongoDataSource.Connect()
  defer c.MongoDataSource.Close(connect)
  collection := c.MongoDataSource.DataSource(connect, c.DatabaseName, c.TableName)
  ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
  defer cancel()

  var mapper *T
  objectID, _ := primitive.ObjectIDFromHex(id)
  filter := bson.D{{Key: "_id", Value: bson.D{{Key: "$eq", Value: objectID}}}}
  err := collection.FindOne(ctx, filter).Decode(&mapper)

  if err != nil {
    return nil, err
  }
  return mapper, nil
}

func (c GenericRepository[T]) FindAllByCustomized(column string, key string) ([]*T, error) {
  connect, _ := c.MongoDataSource.Connect()
  defer c.MongoDataSource.Close(connect)
  collection := c.MongoDataSource.DataSource(connect, c.DatabaseName, c.TableName)
  ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
  defer cancel()

  filter := bson.D{{Key: key, Value: bson.D{{Key: "$eq", Value: column}}}}

  var results []*T
  cursor, err := collection.Find(ctx, filter)

  if err != nil {
    return nil, err
  }
  if err = cursor.All(ctx, &results); err != nil {
    return nil, err
  }
  for _, result := range results {
    _, err := json.MarshalIndent(result, "", "    ")
    if err != nil {
      panic(err)
    }
  }
  return results, nil
}

func (c GenericRepository[T]) FindAll(page int, limit int) (
    *PaginatedData,
    []*T,
    error,
) {
  connect, _ := c.MongoDataSource.Connect()
  defer c.MongoDataSource.Close(connect)
  collection := c.MongoDataSource.DataSource(connect, c.DatabaseName, c.TableName)
  ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
  defer cancel()

  var mappers []*T
  paginatedData, err := New(collection).Context(ctx).Limit(int64(limit)).Filter(bson.M{}).Page(int64(page)).Decode(&mappers).Find()
  if err != nil {
    return nil, nil, err
  }
  return paginatedData, mappers, nil
}

// codition "$or"  ou "$and"
func (c GenericRepository[T]) Search(query framework_mapper.QueryMapper, condition string) ([]*T, error) {
  connect, _ := c.MongoDataSource.Connect()
  defer c.MongoDataSource.Close(connect)
  collection := c.MongoDataSource.DataSource(connect, c.DatabaseName, c.TableName)
  ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
  defer cancel()

  filters := bson.D{
    {Key: condition,
      Value: bson.A{
        bson.D{{Key: "_id", Value: bson.D{{Key: "$eq", Value: query.Uuid}}}},
        bson.D{{Key: "parent_category", Value: bson.D{{Key: "$eq", Value: query.ParentCategory}}}},
        bson.D{{Key: "status", Value: bson.D{{Key: "$eq", Value: query.Status}}}},
        bson.D{{Key: "create_at", Value: bson.D{{Key: "$gte", Value: query.InitialCreationDate}}}},
        bson.D{{Key: "create_at", Value: bson.D{{Key: "$lte", Value: query.FinalCreationDate}}}},
        bson.D{{Key: "update_at", Value: bson.D{{Key: "$gte", Value: query.InitialChangeDate}}}},
        bson.D{{Key: "update_at", Value: bson.D{{Key: "$lte", Value: query.FinalChangeDate}}}},
      },
    },
  }

  var results []*T
  cursor, err := collection.Find(ctx, filters)

  if err != nil {
    return nil, err
  }
  if err = cursor.All(ctx, &results); err != nil {
    return nil, err
  }
  for _, result := range results {
    _, err := json.MarshalIndent(result, "", "    ")
    if err != nil {
      panic(err)
    }
  }
  return results, nil
}
