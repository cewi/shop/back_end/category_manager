package framework_mapper

import (
  "go.mongodb.org/mongo-driver/bson/primitive"
  "time"
)

type QueryMapper struct {
  Uuid                primitive.ObjectID
  InitialCreationDate time.Time
  FinalCreationDate   time.Time
  InitialChangeDate   time.Time
  FinalChangeDate     time.Time
  Status              bool
  ParentCategory      string
}
