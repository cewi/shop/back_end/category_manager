package framework_mapper

import (
  "go.mongodb.org/mongo-driver/bson/primitive"
  "time"
)

type CategoryMapper struct {
  Uuid           primitive.ObjectID `bson:"_id"`
  Description    string             `bson:"description"`
  UpdateAt       time.Time          `bson:"update_at"`
  CreateAt       time.Time          `bson:"create_at"`
  Status         bool               `bson:"status"`
  ParentCategory string             `bson:"parent_category"`
}
