package adapter_controller

import (
  "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/proto_buffer"
  "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/repository/category"
  "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/repository/database"
  "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/repository/mapper"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "google.golang.org/protobuf/types/known/timestamppb"
)

type ICategoryAdapterController interface {
  FindById(queryByIdRequest proto_buffer.QueryByIdRequest) (*proto_buffer.CategoryResponse, error)
  FindAll(queryPaginationRequest proto_buffer.QueryPaginationRequest) (*proto_buffer.CategoriesResponse, error)
  FindByParent(queryByIdRequest proto_buffer.QueryByIdRequest) (*proto_buffer.CategoriesResponse, error)
  Search(querySearchRequest proto_buffer.QuerySearchRequest) (*proto_buffer.CategoriesResponse, error)
}

type categoryAdapterController struct {
  categoryRepository framework_repository.ICategoryRepository
}

func NewCategoryAdapterController() ICategoryAdapterController {
  mongoDataSource := repository_database.NewMongoDataSource()
  repository := framework_repository.NewCategoryRepository(mongoDataSource)
  return &categoryAdapterController{categoryRepository: repository}
}

func (c *categoryAdapterController) FindById(request proto_buffer.QueryByIdRequest) (*proto_buffer.CategoryResponse, error) {
  category, err := c.categoryRepository.FindById(request.Uuid)
  if err != nil {
    return c.toFail(err, int64(1001)), nil
  }
  return c.toResponse(category), nil
}

func (c *categoryAdapterController) FindAll(request proto_buffer.QueryPaginationRequest) (*proto_buffer.CategoriesResponse, error) {
  pagination, err := c.categoryRepository.FindAll(request.Page, request.Size)
  if err != nil {
    return c.toFailAll(err, int64(1002)), nil
  }
  return c.toCategoriesPagination(pagination), nil
}

func (c *categoryAdapterController) FindByParent(request proto_buffer.QueryByIdRequest) (*proto_buffer.CategoriesResponse, error) {
  parent, err := c.categoryRepository.FindByParent(request.Uuid, "parent_category")
  if err != nil {
    return c.toFailAll(err, int64(1003)), nil
  }
  return c.toCategories(parent), nil
}

func (c *categoryAdapterController) Search(request proto_buffer.QuerySearchRequest) (*proto_buffer.CategoriesResponse, error) {
  categories, err := c.categoryRepository.Search(c.toQuery(request))
  if err != nil {
    return c.toFailAll(err, int64(1004)), nil
  }
  return c.toCategories(categories), nil
}

func (c *categoryAdapterController) toQuery(request proto_buffer.QuerySearchRequest) framework_mapper.QueryMapper {
  uuid, _ := primitive.ObjectIDFromHex(request.Uuid)
  return framework_mapper.QueryMapper{
    Uuid:                uuid,
    ParentCategory:      request.UuidParent,
    Status:              request.Status,
    InitialCreationDate: request.InitialCreationDate.AsTime(),
    FinalCreationDate:   request.FinalCreationDate.AsTime(),
    InitialChangeDate:   request.InitialChangeDate.AsTime(),
    FinalChangeDate:     request.FinalChangeDate.AsTime(),
  }
}

func (c *categoryAdapterController) toCategoriesPagination(mapper *framework_mapper.PaginationCategoriesMapper) *proto_buffer.CategoriesResponse {
  return c.toCategories(mapper.Categories)
}

func (c *categoryAdapterController) toCategories(mapper []*framework_mapper.CategoryMapper) *proto_buffer.CategoriesResponse {
  var categories []*proto_buffer.Category
  for _, item := range mapper {
    response := c.toCategory(item)
    categories = append(categories, response)
  }
  response := &proto_buffer.CategoriesResponse{Categories: categories}
  return response
}

func (c *categoryAdapterController) toFail(err error, code int64) *proto_buffer.CategoryResponse {
  return &proto_buffer.CategoryResponse{
    Error: &proto_buffer.ErrorQuery{
      Error: err.Error(),
      Code:  code,
    },
  }
}

func (c *categoryAdapterController) toFailAll(err error, code int64) *proto_buffer.CategoriesResponse {
  return &proto_buffer.CategoriesResponse{
    Error: &proto_buffer.ErrorQuery{
      Error: err.Error(),
      Code:  code,
    },
  }
}

func (c *categoryAdapterController) toResponse(categoryMapper *framework_mapper.CategoryMapper) *proto_buffer.CategoryResponse {
  return &proto_buffer.CategoryResponse{Category: c.toCategory(categoryMapper)}
}

func (c *categoryAdapterController) toCategory(categoryMapper *framework_mapper.CategoryMapper) *proto_buffer.Category {
  return &proto_buffer.Category{
    Uuid:        categoryMapper.Uuid.Hex(),
    Status:      categoryMapper.Status,
    Description: categoryMapper.Description,
    CreateAt:    timestamppb.New(categoryMapper.CreateAt),
    UpdateAt:    timestamppb.New(categoryMapper.UpdateAt),
    OptionalParentCategory: &proto_buffer.Category_ParentCategory{
      ParentCategory: categoryMapper.ParentCategory,
    },
  }
}
