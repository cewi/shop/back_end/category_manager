package adapter_controller

import (
  "github.com/stretchr/testify/assert"
  "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/proto_buffer"
  framework_mapper "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/repository/mapper"
  "gitlab.com/cewi/shop/back_end/category_manager/query/src/interface_adapter"
  mock "gitlab.com/cewi/shop/back_end/category_manager/query/src/interface_adapter/adpter_controller/mock"
  "go.mongodb.org/mongo-driver/bson/primitive"
  "google.golang.org/protobuf/types/known/timestamppb"
  "testing"
  "time"
)

func Test_devera_buscar_por_id(t *testing.T) {
  assertions := assert.New(t)

  categoryAdapterRepositoryMock := new(mock.ICategoryAdapterRepositoryMock)
  adapterController := NewCategoryAdapterController(categoryAdapterRepositoryMock)

  uuid := primitive.NewObjectID()
  updateAt, _ := time.Parse("2006-01-02", "2022-02-01")
  createAt, _ := time.Parse("2006-01-02", "2022-02-01")
  var categoryMapper *framework_mapper.CategoryMapper
  var err *interface_adapter.Error

  categoryMapper = &framework_mapper.CategoryMapper{
    Uuid:           uuid,
    Status:         true,
    UpdateAt:       updateAt,
    CreateAt:       createAt,
    Description:    "Mackbook",
    ParentCategory: "b079ebc9-77cd-45a4-b225-2a6f0ee7d2ac",
  }

  categoryAdapterRepositoryMock.On("FindById").Return(categoryMapper, err)

  categoryResponse, _ := adapterController.FindById(proto_buffer.QueryByIdRequest{
    Uuid: "b079ebc9-77cd-45a4-b225-2a6f0ee7d2ac",
  })

  assertions.NotNil(categoryResponse)
  assertions.Equal(uuid.Hex(), categoryResponse.Category.Uuid)
  assertions.Equal(true, categoryResponse.Category.Status)
  assertions.Equal("Mackbook", categoryResponse.Category.Description)
  assertions.Equal(timestamppb.New(createAt), categoryResponse.Category.CreateAt)
  assertions.Equal(timestamppb.New(updateAt), categoryResponse.Category.UpdateAt)
  categoryAdapterRepositoryMock.AssertNumberOfCalls(t, "FindById", 1)
}

func Test_devera_buscar_por_id_com_erro(t *testing.T) {
  assertions := assert.New(t)

  categoryAdapterRepositoryMock := new(mock.ICategoryAdapterRepositoryMock)
  adapterController := NewCategoryAdapterController(categoryAdapterRepositoryMock)

  var categoryMapper *framework_mapper.CategoryMapper
  newError := interface_adapter.NewError("erro no banco de dados", 2001)

  categoryAdapterRepositoryMock.On("FindById").Return(categoryMapper, &newError)

  response, _ := adapterController.FindById(proto_buffer.QueryByIdRequest{
    Uuid: "b079ebc9-77cd-45a4-b225-2a6f0ee7d2ac",
  })

  assertions.NotNil(response)
  assertions.NotNil(response.Error)
  assertions.Equal("erro no banco de dados", response.Error.Error)
  assertions.Equal(int64(2001), response.Error.Code)
  categoryAdapterRepositoryMock.AssertNumberOfCalls(t, "FindById", 1)

}

func Test_devera_buscar_todos(t *testing.T) {
  assertions := assert.New(t)

  categoryAdapterRepositoryMock := new(mock.ICategoryAdapterRepositoryMock)
  adapterController := NewCategoryAdapterController(categoryAdapterRepositoryMock)

  uuid := primitive.NewObjectID()
  updateAt, _ := time.Parse("2006-01-02", "2022-02-01")
  createAt, _ := time.Parse("2006-01-02", "2022-02-01")
  var categoryMapper *framework_mapper.CategoryMapper
  var err *interface_adapter.Error

  categoryMapper = &framework_mapper.CategoryMapper{
    Uuid:           uuid,
    Status:         true,
    UpdateAt:       updateAt,
    CreateAt:       createAt,
    Description:    "Mackbook",
    ParentCategory: "b079ebc9-77cd-45a4-b225-2a6f0ee7d2ac",
  }

  paginationCategoriesMapper := framework_mapper.PaginationCategoriesMapper{
    Categories: []*framework_mapper.CategoryMapper{
      categoryMapper,
    },
  }

  categoryAdapterRepositoryMock.On("FindAll").Return(&paginationCategoriesMapper, err)

  categoryResponse, _ := adapterController.FindAll(proto_buffer.QueryPaginationRequest{
    Page: 0,
    Size: 0,
  })

  assertions.NotNil(categoryResponse)
  assertions.NotNil(categoryResponse.Categories)
  assertions.Len(categoryResponse.Categories, 1)
  assertions.Equal(uuid.Hex(), categoryResponse.Categories[0].Uuid)
  assertions.Equal(true, categoryResponse.Categories[0].Status)
  assertions.Equal("Mackbook", categoryResponse.Categories[0].Description)
  assertions.Equal(timestamppb.New(createAt), categoryResponse.Categories[0].CreateAt)
  assertions.Equal(timestamppb.New(updateAt), categoryResponse.Categories[0].UpdateAt)
  categoryAdapterRepositoryMock.AssertNumberOfCalls(t, "FindAll", 1)
}

func Test_devera_buscar_todos_com_error(t *testing.T) {
  assertions := assert.New(t)

  categoryAdapterRepositoryMock := new(mock.ICategoryAdapterRepositoryMock)
  adapterController := NewCategoryAdapterController(categoryAdapterRepositoryMock)

  newError := interface_adapter.NewError("erro no banco de dados", 2001)

  var paginationCategoriesMapper *framework_mapper.PaginationCategoriesMapper

  categoryAdapterRepositoryMock.On("FindAll").Return(paginationCategoriesMapper, &newError)

  categoryResponse, _ := adapterController.FindAll(proto_buffer.QueryPaginationRequest{
    Page: 0,
    Size: 0,
  })

  assertions.NotNil(categoryResponse)
  assertions.NotNil(categoryResponse.Error)
  assertions.Equal("erro no banco de dados", categoryResponse.Error.Error)
  assertions.Equal(int64(2001), categoryResponse.Error.Code)
  categoryAdapterRepositoryMock.AssertNumberOfCalls(t, "FindAll", 1)
}

func Test_devera_buscar_por_id_do_pai(t *testing.T) {
  assertions := assert.New(t)

  categoryAdapterRepositoryMock := new(mock.ICategoryAdapterRepositoryMock)
  adapterController := NewCategoryAdapterController(categoryAdapterRepositoryMock)

  uuid := primitive.NewObjectID()
  updateAt, _ := time.Parse("2006-01-02", "2022-02-01")
  createAt, _ := time.Parse("2006-01-02", "2022-02-01")
  var categoryMapper *framework_mapper.CategoryMapper
  var err *interface_adapter.Error

  categoryMapper = &framework_mapper.CategoryMapper{
    Uuid:           uuid,
    Status:         true,
    UpdateAt:       updateAt,
    CreateAt:       createAt,
    Description:    "Mackbook",
    ParentCategory: "b079ebc9-77cd-45a4-b225-2a6f0ee7d2ac",
  }

  var categories = []*framework_mapper.CategoryMapper{categoryMapper}

  categoryAdapterRepositoryMock.On("FindByParent").Return(categories, err)

  categoryResponse, _ := adapterController.FindByParent(proto_buffer.QueryByIdRequest{
    Uuid: "b079ebc9-77cd-45a4-b225-2a6f0ee7d2ac",
  })

  assertions.NotNil(categoryResponse)
  assertions.NotNil(categoryResponse.Categories)
  assertions.Len(categoryResponse.Categories, 1)
  assertions.Equal(uuid.Hex(), categoryResponse.Categories[0].Uuid)
  assertions.Equal(true, categoryResponse.Categories[0].Status)
  assertions.Equal("Mackbook", categoryResponse.Categories[0].Description)
  assertions.Equal(timestamppb.New(createAt), categoryResponse.Categories[0].CreateAt)
  assertions.Equal(timestamppb.New(updateAt), categoryResponse.Categories[0].UpdateAt)
  categoryAdapterRepositoryMock.AssertNumberOfCalls(t, "FindByParent", 1)
}

func Test_devera_buscar_por_id_do_pai_com_erros(t *testing.T) {
  assertions := assert.New(t)

  categoryAdapterRepositoryMock := new(mock.ICategoryAdapterRepositoryMock)
  adapterController := NewCategoryAdapterController(categoryAdapterRepositoryMock)
  err := interface_adapter.NewError("erro com o banco de dados", 2001)
  var categories []*framework_mapper.CategoryMapper

  categoryAdapterRepositoryMock.On("FindByParent").Return(categories, &err)

  categoryResponse, _ := adapterController.FindByParent(proto_buffer.QueryByIdRequest{
    Uuid: "b079ebc9-77cd-45a4-b225-2a6f0ee7d2ac",
  })

  assertions.NotNil(categoryResponse)
  assertions.NotNil(categoryResponse.Error)
  assertions.Equal("erro com o banco de dados", categoryResponse.Error.Error)
  assertions.Equal(int64(2001), categoryResponse.Error.Code)
  categoryAdapterRepositoryMock.AssertNumberOfCalls(t, "FindByParent", 1)
}

func Test_devera_buscar_com_filtros(t *testing.T) {
  assertions := assert.New(t)

  categoryAdapterRepositoryMock := new(mock.ICategoryAdapterRepositoryMock)
  adapterController := NewCategoryAdapterController(categoryAdapterRepositoryMock)

  uuid := primitive.NewObjectID()
  updateAt, _ := time.Parse("2006-01-02", "2022-02-01")
  createAt, _ := time.Parse("2006-01-02", "2022-02-01")
  var categoryMapper *framework_mapper.CategoryMapper
  var err *interface_adapter.Error

  categoryMapper = &framework_mapper.CategoryMapper{
    Uuid:           uuid,
    Status:         true,
    UpdateAt:       updateAt,
    CreateAt:       createAt,
    Description:    "Mackbook",
    ParentCategory: "b079ebc9-77cd-45a4-b225-2a6f0ee7d2ac",
  }

  var categories = []*framework_mapper.CategoryMapper{categoryMapper}

  categoryAdapterRepositoryMock.On("Search").Return(categories, err)

  categoryResponse, _ := adapterController.Search(proto_buffer.QuerySearchRequest{
    Uuid:                "b079ebc9-77cd-45a4-b225-2a6f0ee7d2ac",
    Status:              true,
    InitialCreationDate: timestamppb.New(createAt),
    FinalCreationDate:   timestamppb.New(createAt),
    InitialChangeDate:   timestamppb.New(updateAt),
    FinalChangeDate:     timestamppb.New(updateAt),
  })

  assertions.NotNil(categoryResponse)
  assertions.NotNil(categoryResponse.Categories)
  assertions.Len(categoryResponse.Categories, 1)
  assertions.Equal(uuid.Hex(), categoryResponse.Categories[0].Uuid)
  assertions.Equal(true, categoryResponse.Categories[0].Status)
  assertions.Equal("Mackbook", categoryResponse.Categories[0].Description)
  assertions.Equal(timestamppb.New(createAt), categoryResponse.Categories[0].CreateAt)
  assertions.Equal(timestamppb.New(updateAt), categoryResponse.Categories[0].UpdateAt)
  categoryAdapterRepositoryMock.AssertNumberOfCalls(t, "Search", 1)
}

func Test_devera_buscar_com_filtros_com_erro(t *testing.T) {
  assertions := assert.New(t)

  categoryAdapterRepositoryMock := new(mock.ICategoryAdapterRepositoryMock)
  adapterController := NewCategoryAdapterController(categoryAdapterRepositoryMock)

  updateAt, _ := time.Parse("2006-01-02", "2022-02-01")
  createAt, _ := time.Parse("2006-01-02", "2022-02-01")

  err := interface_adapter.NewError("erro com o banco de dados", 2001)

  var categories []*framework_mapper.CategoryMapper

  categoryAdapterRepositoryMock.On("Search").Return(categories, &err)

  categoryResponse, _ := adapterController.Search(proto_buffer.QuerySearchRequest{
    Uuid:                "b079ebc9-77cd-45a4-b225-2a6f0ee7d2ac",
    Status:              true,
    InitialCreationDate: timestamppb.New(createAt),
    FinalCreationDate:   timestamppb.New(createAt),
    InitialChangeDate:   timestamppb.New(updateAt),
    FinalChangeDate:     timestamppb.New(updateAt),
  })

  assertions.NotNil(categoryResponse)
  assertions.NotNil(categoryResponse.Error)
  assertions.Equal("erro com o banco de dados", categoryResponse.Error.Error)
  assertions.Equal(int64(2001), categoryResponse.Error.Code)
  categoryAdapterRepositoryMock.AssertNumberOfCalls(t, "Search", 1)
}
