package mock

import (
  "github.com/stretchr/testify/mock"
  framework_mapper "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/repository/mapper"
  "gitlab.com/cewi/shop/back_end/category_manager/query/src/interface_adapter"
)

type ICategoryAdapterRepositoryMock struct {
  mock.Mock
}

func (mock *ICategoryAdapterRepositoryMock) FindById(uuid string) (*framework_mapper.CategoryMapper, *interface_adapter.Error) {
  args := mock.Called()
  result := args.Get(0)
  return result.(*framework_mapper.CategoryMapper), args.Error(1).(*interface_adapter.Error)
}

func (mock *ICategoryAdapterRepositoryMock) FindAll(page int64, size int64) (*framework_mapper.PaginationCategoriesMapper, *interface_adapter.Error) {
  args := mock.Called()
  result := args.Get(0)
  return result.(*framework_mapper.PaginationCategoriesMapper), args.Get(1).(*interface_adapter.Error)
}

func (mock *ICategoryAdapterRepositoryMock) FindByParent(uuid string) ([]*framework_mapper.CategoryMapper, *interface_adapter.Error) {
  args := mock.Called()
  return args.Get(0).([]*framework_mapper.CategoryMapper), args.Get(1).(*interface_adapter.Error)
}

func (mock *ICategoryAdapterRepositoryMock) Search(query framework_mapper.QueryMapper) ([]*framework_mapper.CategoryMapper, *interface_adapter.Error) {
  args := mock.Called()
  result := args.Get(0)
  return result.([]*framework_mapper.CategoryMapper), args.Error(1).(*interface_adapter.Error)
}
