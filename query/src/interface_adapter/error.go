package interface_adapter

import "errors"

type Error struct {
  error
  code int
}

func (e Error) Error() string {
  return e.error.Error()
}

func (e Error) Code() int {
  return e.code
}

func NewError(message string, code int) Error {
  return Error{
    error: errors.New(message),
    code:  code,
  }
}
