package src

import (
  framework_controller "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/controller"

  "gitlab.com/cewi/shop/back_end/category_manager/query/src/framework/proto_buffer"
  "google.golang.org/grpc"
  "google.golang.org/grpc/reflection"
  "log"
  "net"
  "os"
)

type application struct {
  categoryController proto_buffer.CategoryQueryServer
}

const (
  TCP = "tcp"
)

func (a *application) Init() {

  grpcServer := grpc.NewServer()
  proto_buffer.RegisterCategoryQueryServer(grpcServer, a.categoryController)
  reflection.Register(grpcServer)

  port := os.Getenv("GRPC_PORT")

  listener, err := net.Listen(TCP, ":"+port)
  if err != nil {
    log.Fatalf(err.Error())
  }
  log.Printf("Applicação Category Query, inicializada com sucesso na porta: %v\n", port)
  errs := grpcServer.Serve(listener)
  if errs != nil {
    panic(errs)
  }
}

func NewApplication() *application {
  categoryController := framework_controller.NewCategoryController()
  log.Println("Serviço Category query inicializado")
  return &application{categoryController: &categoryController}
}
