import grpc from 'k6/net/grpc';
import {check, sleep} from 'k6';

const client = new grpc.Client();
client.load(['../../proto'], 'category_query.proto');

export const options = {
  ext: {
    loadimpact: {
      projectID: 3603316,
      name: "find_by_parent_streess_1"
    }
  },
  stages: [
    {duration: '10s', target: 45},
    {duration: '1m', target: 45},
    {duration: '10s', target: 50},
    {duration: '3m', target: 50},
    {duration: '10s', target: 30},
    {duration: '3m', target: 30},
    {duration: '10s', target: 0},
  ],
  thresholds: {
    grpc_req_duration: [{threshold: 'p(100)<3000'}],
    iteration_duration: [{threshold: 'p(100)<10000'}],
  },
};

export default function () {

  client.connect("host.docker.internal:9003", {plaintext: true, reflect: true, timeout: "10s"});

  const data = {uuid: "633da012abd93d795b3b9f7a"};
  const response = client.invoke("category_query.CategoryQuery/FindByParent", data);
  check(response, {"status is ok": (r) => r && r.status === grpc.StatusOK});

  const response2 = client.invoke("category_query.CategoryQuery/FindByParent", data);
  check(response2, {"status is ok": (r) => r && r.status === grpc.StatusOK});

  client.close();
  sleep(1);
};