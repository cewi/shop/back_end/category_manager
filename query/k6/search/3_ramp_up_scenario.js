import grpc from 'k6/net/grpc';
import {check, sleep} from 'k6';

const client = new grpc.Client();
client.load(['../../proto'], 'category_query.proto');

export const options = {
  ext: {
    loadimpact: {
      projectID: 3603316,
      name: "search_ramp_up_scenario"
    }
  },
  stages: [
    {duration: '2m', target: 30},
    {duration: '3m', target: 30},
    {duration: '1m', target: 50},
    {duration: '1m', target: 50},
    {duration: '1m', target: 30},
    {duration: '3m', target: 30},
    {duration: '1m', target: 0},
  ],
  thresholds: {
    grpc_req_duration: [{threshold: 'p(99)<1500'}],
    iteration_duration: [{threshold: 'p(99)<1500'}],
  },
};

export default function () {

  client.connect("host.docker.internal:9003", {plaintext: true, reflect: true, timeout: "10s"});

  const request = {
    uuid: "633de41cfba783d415873840",
    uuid_parent: "ec67570d-8047-4a19-be98-496dac95c81c",
    status: true,
  }
  const response = client.invoke("category_query.CategoryQuery/Search", request);

  check(response, {"status is ok": (r) => r && r.status === grpc.StatusOK});

  client.close();
  sleep(1);
};