import grpc from 'k6/net/grpc';
import {check, sleep} from 'k6';

const client = new grpc.Client();
client.load(['../../proto'], 'category_query.proto');

export const options = {
  ext: {
    loadimpact: {
      projectID: 3603316,
      name: "create_ramp_up_scenario"
    }
  },
  stages: [
    {duration: '2m', target: 30},
    {duration: '3m', target: 30},
    {duration: '1m', target: 50},
    {duration: '1m', target: 50},
    {duration: '1m', target: 30},
    {duration: '3m', target: 30},
    {duration: '1m', target: 0},
  ],
  thresholds: {
    grpc_req_duration: [{threshold: 'p(99)<1500'}],
    iteration_duration: [{threshold: 'p(99)<1500'}],
  },
};

export default function () {

  client.connect("host.docker.internal:9003", {plaintext: true, reflect: true, timeout: "10s"});

  const data = {uuid: "633da012abd93d795b3b9f7a"};
  const response = client.invoke("category_query.CategoryQuery/FindById", data);

  check(response, {"status is ok": (r) => r && r.status === grpc.StatusOK});

  client.close();
  sleep(1);
};