import grpc from "k6/net/grpc";
import {check, sleep} from "k6";

const client = new grpc.Client();
client.load(["../../proto"], "category_query.proto");

export const options = {
  ext: {
    loadimpact: {
      projectID: 3603316,
      name: "find_by_id_smoke",
    },
  },
  vus: 1,
  duration: "1m",
  thresholds: {
    grpc_req_duration: [{threshold: "p(100)<1500"}],
    iteration_duration: [{threshold: "p(100)<1500"}],
  },
};

export default function () {
  client.connect("host.docker.internal:9003", {plaintext: true, reflect: true, timeout: "10s"});

  const data = {uuid: "633de41cfba783d415873840"};
  const response = client.invoke("category_query.CategoryQuery/FindById", data);

  check(response, {"status is ok": (r) => r && r.status === grpc.StatusOK});

  client.close();
  sleep(1);
}
