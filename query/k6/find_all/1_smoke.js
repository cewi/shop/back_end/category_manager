import grpc from "k6/net/grpc";
import {check, fail} from 'k6';

const client = new grpc.Client();
client.load(["../../proto"], "category_query.proto");

export const options = {
  ext: {
    loadimpact: {
      projectID: 3603316,
      name: "find_all_smoke",
    },
  },
  vus: 50,
  duration: "1m",
  thresholds: {
    grpc_req_duration: [{threshold: "p(100)<1500"}],
    iteration_duration: [{threshold: "p(100)<1500"}],
  },
};

export default function () {
  client.connect('host.docker.internal:9002', {
    plaintext: true,
    reflect: true,
    timeout: "10s"
  });


  const request = {page: 0, size: 0}
  const res = client.invoke("category_query.CategoryQuery/FindAll", request);

  check(res, {
    "status is ok": (r) => r && r.status === grpc.StatusOK,
    "size": (r) => r && r.message.categories.length > 0
  });

  if (!res) {
    fail('unexpected res');
  }

  client.close();
}
