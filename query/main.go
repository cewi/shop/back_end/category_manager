package main

import (
  "gitlab.com/cewi/shop/back_end/category_manager/query/src"
  "log"
)

func main() {
  log.Println("Inicializando serviço Category query")
  src.NewApplication().Init()
}
